<?php

    class db{
        private $dbhost = 'localhost';
        private $dbUser = 'root';
        private $dbPass = '';
        private $dbName = 'apirest';

        //coneccion
        public function conecctionDB(){
            $mysqlConnect = "mysql:host=$this->dbhost;dbname=$this->dbName";
            $dbConnecion = new PDO($mysqlConnect, $this->dbUser, $this->dbPass);
            $dbConnecion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $dbConnecion;
        }
    }