<?php

require '../vendor/autoload.php';
require '../src/config/db.php';

$app = new \Slim\App;


//ruta clientes
require '../src/rutas/clientes.php';

// Run app
$app->run();